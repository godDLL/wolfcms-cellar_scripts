<?php

/* Security measure */
if (!defined('IN_CMS')) { exit(); }

if (false === Plugin::getSetting('show_tab', 'cellar_scripts')) {

  $settings = array(
  );

  Plugin::setAllSettings($settings, 'cellar_scripts');
}
