<?php

/* Security measure */
if (!defined('IN_CMS')) { exit(); }

class CellarScriptsController extends PluginController {

    public function __construct() {
        $this->setLayout('backend');
    }

    public function index() {
        $this->documentation();
    }

    public function documentation() {
        $this->display('cellar_scripts/views/help');
    }

    function settings() {
        $this->display('cellar_scripts/views/settings', Plugin::getAllSettings('cellar_scripts'));
    }
}

