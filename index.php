<?php

/* Security measure */
if (!defined('IN_CMS')) { exit(); }

Plugin::setInfos(array(
    'id'          => 'cellar_scripts',
    'title'       => __('Cellar Scripts'),
    'type' => 'both',
    'version'     => '0.1.0',
    'license'     => 'GPL',
    'author'      => 'Yuli Che.',
    'website'     => 'https://bitbucket.org/pan-gap/wolfcms-cellar_scripts',
    'update_url'  => 'https://bitbucket.org/godDLL/wolfcms-cellar_scripts/raw/master/plugin-versions.xml',
    'require_wolf_version' => '0.8.3'
));

Plugin::addController('cellar_scripts', __('Cellar Scripts'), 'administrator',
    Plugin::getSetting('show_tab', 'cellar_scripts') ? true : false
);

