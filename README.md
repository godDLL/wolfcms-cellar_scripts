# Download

To get a stable version with all the latest changes [download :master](https://bitbucket.org/godDLL/wolfcms-cellar_scripts/get/master.zip)

All [historical versions](https://bitbucket.org/godDLL/wolfcms-cellar_scripts/downloads#tag-downloads) of the plugin are available.


# Installation

Make sure the folder you downloaded includes the file `index.php`  
and is named exactly `cellar_scripts`. Put it in your `wolf/plugins/`  
folder, so that it sits at `wolf/plugins/cellar_scripts/index.php`  

In the Administration tab click on a checkbox to enable this plugin.


# Features

# TODO

## 0.1.0


# Bugs and Improvements

When you see some issue, or have an idea for improvement please  
search the [Issue tracker](https://bitbucket.org/godDLL/wolfcms-cellar_scripts/issues), and possibly create an Issue.

If your idea or bug comes with working code, please [make a PR](https://bitbucket.org/godDLL/wolfcms-cellar_scripts/pull-requests/) onto the `:develop` branch.

Code license is GPLv2, where applicable.
